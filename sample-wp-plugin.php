<?php
/*
Plugin Name: Sample WP Plugin
Plugin URI: https://gitlab.com/mrkrstphr/sample-wp-plugin
Description: This is just a test
Author: Kristopher Wilson
Version: 1.0
License: MIT
*/

function footer_notice() {
    echo '<p>I\'m all up in yer footer!</p>';
}

add_action('wp_footer', 'footer_notice');
